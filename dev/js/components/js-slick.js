(function(){    
    $('.slider-wrapper').on('init', function() {
        $('.slider-wrapper').addClass('active');
    });

    $('.slider-wrapper').slick({
        infinite: false,
        slidesToShow: 3.5,
        slidesToScroll: 1,
        dots: true,
        arrows: true,
        prevArrow: '.slider-prev',
        nextArrow: '.slider-next',
        speed: 1000,
        cssEase: 'ease-in-out',
        autoplay: false,
        swipeToSlide: true,
        autoplaySpeed: 2000,
        variableWidth: true,
        responsive: [
            {
                breakpoint: 1400,
                settings: {
                    slidesToShow: 4.4
                }
            },
            {
                breakpoint: 1262,
                settings: {
                    slidesToShow: 3.9
                }
            },
            {
                breakpoint: 1040,
                settings: {
                    slidesToShow: 3.7
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 3.7,
                    dots: false
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2.8,
                    dots: false
                }
            },
            {
                breakpoint: 580,
                settings: {
                    slidesToShow: 2,
                    dots: false
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1.7,
                    dots: false
                }
            },
            {
                breakpoint: 380,
                settings: {
                    slidesToShow: 1.3,
                    dots: false
                }
            }
        ]
    });
})();
